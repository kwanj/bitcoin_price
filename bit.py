import requests


BITCOIN_API_URL = 'https://api.coinmarketcap.com/v1/ticker/bitcoin/'

def get_latest_bitcoin_price():
    response = requests.get(BITCOIN_API_URL)
    response_json = response.json()
    return float(response_json[0]['price_usd'])  # Convert the price to a floating point number

print(get_latest_bitcoin_price())